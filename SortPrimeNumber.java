package simples;

import java.util.Arrays;

public class SortPrimeNumber {

    public static void main(String[] args) {
        int[] array = {1, 90, 54, 32, 25, 8, 112, 53, 10};

        System.out.println(Arrays.toString(sortedBy(array)));
    }

    public static int[] sortedBy(int[] array) {
        for (int i = 0; i < (array.length); i++) {
            int first = array[i];
            int firstPrime = first % 2;
            for(int j = array.length - 1; j > 0; j--){
                int last = array[j];
                int lastPrime = last % 2;
                if(i > ((array.length - 2) / 2)){
                    break;
                }
                if(firstPrime != 0 && lastPrime == 0) {
                    int tmp = first;
                    array[i] = last;
                    array[j] = tmp;
                    break;
                }
            }
        }
        return array;
    }

}
