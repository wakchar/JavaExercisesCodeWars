package simples;

public class RomanNumeralsEncoder {

    private static int[] arabNumber = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
    private static String[] romanArray = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X",
            "IX", "V", "IV", "I"};

    public static void main(String[] args) {
        System.out.println(solution(1000));
    }

    private static String solution(int n) {
        String solution = "";
        StringBuilder solutionTmp = new StringBuilder();
        int i = 0;
        while (n > 0) {
            if (arabNumber[i] >= n && n != arabNumber[i]) {
                i++;
            }
            if (n - arabNumber[i] < 0) {
                while (true) {
                    if (n - arabNumber[i] < 0) {
                        i++;
                    } else break;
                }
            }
            int divisor = arabNumber[i];
            n -= divisor;
            solutionTmp.append(romanArray[i]);
        }
        return solutionTmp.toString();
    }
}
//I          1
//        V          5
//        X          10
//        L          50
//        C          100
//        D          500
//        M          1,000
