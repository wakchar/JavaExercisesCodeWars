package simples;

class LastDigitMain {

    public static void main(String[] args) {

        System.out.println(lastDigit(5));

    }

    public static int lastDigit(int n) {
        int[] digitSequence = new int[n + 1];
        digitSequence[1] = 1;
        int lastNumberOfDigitalSequence = fibArray(digitSequence, n);
        while (lastNumberOfDigitalSequence > 10) {
            lastNumberOfDigitalSequence %= 10;
        }
        return lastNumberOfDigitalSequence;
    }

    private static int fibArray(int[] digitArr, int n) {
        if (n == 0 || digitArr[n] > 0) {
            return digitArr[n];
        }
        digitArr[n] = fibArray(digitArr, n - 1) + fibArray(digitArr, n - 2);
        return digitArr[n];
    }
}
