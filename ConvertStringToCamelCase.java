package simples;

import java.util.Arrays;
import java.util.TooManyListenersException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ConvertStringToCamelCase {
    public static void main(String[] args) {

        String str = "the-stealth-warrior";
        System.out.println(toCamelCase(str));
    }

    static String toCamelCase(String s){
        StringBuilder tmp = new StringBuilder();
        char[] filter = s.toCharArray();
        for(int i = 0; i < filter.length; i++){
            if(filter[i] == 45 || filter[i] == 95){
                filter[i + 1] = Character.toUpperCase(filter[i + 1]);
            }
            if(filter[i] != 45 && filter[i] != 95){
                tmp.append(filter[i]);
            }
        }
        return tmp.toString();
    }
}
