package simples;

public class Sravni {
    public static void main(String[] args) {
        try {
            foo();
            System.out.println("a ");
            throw new Exception();
        } catch (Exception e) {
            System.out.println("B ");
        } finally {
            System.out.println("C");
        }
    }

    private static void foo() throws Exception {
       try {
        System.out.println("a1 ");
        throw new Exception();
    } catch (Exception e) {
        System.out.println("B1 ");
    } finally {
        System.out.println("C1");
    }
    }
}
