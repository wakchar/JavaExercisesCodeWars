package simples;

public class Fruit {
    public Fruit() {
        System.out.println("Constr of fruit");
    }
    void method() {
        System.out.println("method of fruit");
    }
    public static void main(String[] args){
        Fruit f = new Apple();
        f.method();
    }
}

class Apple extends Fruit{
    public Apple() {
        System.out.println("Constructor of  simples.Apple");
    }
    void method() {
        System.out.println("method of simples.Apple");
    }
}