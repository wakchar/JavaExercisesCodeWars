package simples;

import java.util.ArrayList;
import java.util.List;

class checkAvailabilityMain {

    public static void main(String[] args) {

        List<String> unAvailable = new ArrayList<>();
        //test1
        unAvailable.add("09:30-10:15");
        unAvailable.add("12:20-15:50");
        checkAvailability(unAvailable, "11:00");
        //test2
//        unAvailable.add("09:30-10:15");
//        unAvailable.add("12:20-15:50");
//        checkAvailability(unAvailable, "10:00");
        //test3
//        unAvailable.add("07:40-07:50");
//        unAvailable.add("07:50-08:00");
//        checkAvailability(unAvailable, "7:45");
    }

    public static String checkAvailability(List<String> schedule, String currentTime) {
        String availableStatus = "";
        for (String s : schedule) {
            String[] beginEndTimeUnavailable = s.split("-");
            if (!(beginEndTimeUnavailable[0].compareTo(currentTime) > 0
                    || beginEndTimeUnavailable[1].compareTo(currentTime) <= 0)) {
                availableStatus = "available";
            } else {
                availableStatus = beginEndTimeUnavailable[1];
                break;
            }
        }
        System.out.println(availableStatus);
        return availableStatus;
    }

}




