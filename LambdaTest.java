package simples;

import java.util.Arrays;

public class LambdaTest {
    public static void main(String[] args){

        String names = "Tror Gvigris Deriana Nori";
        System.out.println(solution(names));

    }

    public static int solution(String names) {
        int value = 0;
        value = (int) Arrays.stream(names.split(" ")).filter(s -> {
            return s.length() < 5;
        }).count();
        return value;
    }
}
